import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taskmanage/src/domain/repo/group_repo.dart';
import 'package:taskmanage/src/domain/repo/invite_repo.dart';
import 'package:taskmanage/src/domain/repo/task_repo.dart';
import 'package:taskmanage/src/domain/repo/user_repo.dart';
import 'package:taskmanage/src/firebase_store/src/group_store.dart';
import 'package:taskmanage/src/firebase_store/src/invite_store.dart';
import 'package:taskmanage/src/firebase_store/src/task_store.dart';
import 'package:taskmanage/src/firebase_store/src/user_store.dart';
import 'package:taskmanage/src/repo/repo.dart';

import 'app.dart';

void launch() async {
  // Ensure initialized
  WidgetsFlutterBinding.ensureInitialized();

  // Initilize Firebase
  await Firebase.initializeApp();

  // Create firebase services
  final firebaseAuth = FirebaseAuth.instance;
  final firestore = FirebaseFirestore.instance;

  // Create stores
  final userFirestore = UserFirestore(
    firebaseAuth: firebaseAuth,
    firestore: firestore,
  );
  final groupFirestore = GroupFirestore(
    firebaseAuth: firebaseAuth,
    firestore: firestore,
  );
  final taskFirestore = TaskFirestore(
    firestore: firestore,
  );
  final inviteFirestore = InviteFirestore(
    firestore: firestore,
  );

  // Create repos
  final userRepo = UserRepo(userFirestore: userFirestore);
  final groupRepo = GroupRepo(groupFirestore: groupFirestore);
  final taskRepo = TaskRepo(taskFirestore: taskFirestore);
  final inviteRepo = InviteRepo(inviteFirestore: inviteFirestore);

  final user = await userRepo.getCurrentUser();

  runZonedGuarded(
    () => runApp(
      MultiRepositoryProvider(
        providers: [
          RepositoryProvider<IUserRepo>(create: (_) => userRepo),
          RepositoryProvider<IGroupRepo>(create: (_) => groupRepo),
          RepositoryProvider<ITaskRepo>(create: (_) => taskRepo),
          RepositoryProvider<IInviteRepo>(create: (_) => inviteRepo),
        ],
        child: App(user: user),
      ),
    ),
    _handleError,
  );
}

void _handleError(Object error, StackTrace stackTrace) {
  print(error);
}
