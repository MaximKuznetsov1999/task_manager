import 'package:taskmanage/src/domain/model/group.dart';
import 'package:taskmanage/src/domain/repo/group_repo.dart';
import 'package:taskmanage/src/firebase_store/src/group_store.dart';

class GroupRepo implements IGroupRepo {
  final GroupFirestore _groupFirestore;

  const GroupRepo({required GroupFirestore groupFirestore})
      : _groupFirestore = groupFirestore;

  @override
  Future<void> createGroup({required String groupName}) {
    return _groupFirestore.createGroup(groupName: groupName);
  }

  @override
  Future<List<Group>> getGroups() {
    return _groupFirestore.getGroups();
  }

  @override
  Future<void> removeWorker(String groupId, String workerId) {
    return _groupFirestore.removeWorker(groupId, workerId);
  }

  @override
  Future<void> removeGroup(String groupId) {
    return _groupFirestore.removeGroup(groupId);
  }

  @override
  Future<void> updateGroupName(String groupId, String userId, String name) {
    return _groupFirestore.updateGroupName(groupId, userId, name);
  }
}
