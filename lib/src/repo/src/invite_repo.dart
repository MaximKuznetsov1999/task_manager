import 'package:taskmanage/src/domain/model/invite.dart';
import 'package:taskmanage/src/domain/repo/invite_repo.dart';
import 'package:taskmanage/src/firebase_store/src/invite_store.dart';

class InviteRepo implements IInviteRepo {
  final InviteFirestore _inviteFirestore;

  const InviteRepo({required InviteFirestore inviteFirestore})
      : _inviteFirestore = inviteFirestore;

  @override
  Future<void> createInvite(
    String ownerId,
    String workerId,
    String groupId,
    String groupName,
  ) {
    return _inviteFirestore.createInvite(ownerId, workerId, groupId, groupName);
  }

  @override
  Future<List<Invite>> getInvites(String userId) {
    return _inviteFirestore.getInvites(userId);
  }
}
