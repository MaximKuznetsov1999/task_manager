import 'package:taskmanage/src/domain/model/task.dart';
import 'package:taskmanage/src/domain/repo/task_repo.dart';
import 'package:taskmanage/src/firebase_store/src/task_store.dart';

class TaskRepo implements ITaskRepo {
  final TaskFirestore _taskFirestore;

  const TaskRepo({required TaskFirestore taskFirestore})
      : _taskFirestore = taskFirestore;

  @override
  Future<void> createTask(String name, String workerId, String description) {
    return _taskFirestore.createTask(name, workerId, description);
  }

  @override
  Future<List<Task>> getTasks(String groupId, String userId) {
    return _taskFirestore.getTasks(groupId, userId);
  }

  @override
  Future<void> removeTask(String taskId) {
    return _taskFirestore.removeTask(taskId);
  }

  @override
  Future<void> updateTask({
    required String id,
    String? name,
    bool? status,
    String? description,
    String? workerId,
  }) {
    return _taskFirestore.updateTask(
      id: id,
      name: name,
      status: status,
      description: description,
      executorId: workerId,
    );
  }
}
