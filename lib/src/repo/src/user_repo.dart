import 'package:taskmanage/src/domain/model/user.dart';
import 'package:taskmanage/src/domain/repo/user_repo.dart';
import 'package:taskmanage/src/firebase_store/src/user_store.dart';

class UserRepo implements IUserRepo {
  final UserFirestore _userFirestore;

  const UserRepo({required UserFirestore userFirestore})
      : _userFirestore = userFirestore;

  @override
  Future<void> createAccount({
    required String email,
    required String name,
    required String password,
  }) {
    return _userFirestore.createAccount(
      email: email,
      name: name,
      password: password,
    );
  }

  @override
  Future<void> login({required String email, required String password}) {
    return _userFirestore.login(email: email, password: password);
  }

  @override
  Future<bool> isVerified() async {
    return _userFirestore.isVerified();
  }

  @override
  Future<User?> getCurrentUser() {
    return _userFirestore.getCurrentUser();
  }

  @override
  Future<void> logout() {
    return _userFirestore.logout();
  }

  @override
  Future<User?> getUserById(String userId) {
    return _userFirestore.getUserById(userId);
  }

  @override
  Future<User?> getUserByEmail(String email) {
    return _userFirestore.getUserByEmail(email);
  }
}
