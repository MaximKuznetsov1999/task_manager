import 'package:flutter/material.dart';
import 'package:taskmanage/src/presentation/navigation/page/navigation_page.dart';

import 'domain/model/user.dart';

class App extends StatelessWidget {
  final User? user;

  App({required this.user});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: NavigationPage(
        user: user,
      ),
    );
  }
}
