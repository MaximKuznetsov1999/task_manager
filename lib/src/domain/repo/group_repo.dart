import 'package:taskmanage/src/domain/model/group.dart';

abstract class IGroupRepo {
  Future<void> createGroup({required String groupName});
  Future<List<Group>> getGroups();
  Future<void> updateGroupName(String groupId, String userId, String name);
  Future<void> removeWorker(String groupId, String workerId);
  Future<void> removeGroup(String groupId);
}
