import 'package:taskmanage/src/domain/model/invite.dart';

abstract class IInviteRepo {
  Future<List<Invite>> getInvites(String userId);
  Future<void> createInvite(
    String ownerId,
    String workerId,
    String groupId,
    String groupName,
  );
}
