import 'package:taskmanage/src/domain/model/task.dart';

abstract class ITaskRepo {
  Future<List<Task>> getTasks(String groupId, String userId);
  Future<void> createTask(String name, String workerId, String description);
  Future<void> updateTask({
    required String id,
    String? name,
    bool? status,
    String? description,
    String? workerId,
  });
  Future<void> removeTask(String taskId);
}
