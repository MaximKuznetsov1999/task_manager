import 'package:taskmanage/src/domain/model/user.dart';

abstract class IUserRepo {
  Future<void> createAccount({
    required String email,
    required String name,
    required String password,
  });
  Future<bool> isVerified();
  Future<void> login({required String email, required String password});
  Future<User?> getCurrentUser();
  Future<void> logout();
  Future<User?> getUserById(String userId);
  Future<User?> getUserByEmail(String email);
}
