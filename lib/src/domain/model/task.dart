class Task {
  final String id;
  final String name;
  final String workerId;
  final String description;
  final bool status;
  final DateTime dateTime;

  const Task({
    required this.id,
    required this.name,
    required this.workerId,
    required this.description,
    required this.status,
    required this.dateTime,
  });
}
