class Invite {
  final String id;
  final String ownerId;
  final String workerId;
  final String groupId;
  final String groupName;

  const Invite({
    required this.id,
    required this.ownerId,
    required this.workerId,
    required this.groupId,
    required this.groupName,
  });
}
