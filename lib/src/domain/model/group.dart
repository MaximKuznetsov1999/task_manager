class Group {
  final String id;
  final String ownerId;
  final String name;
  final List<String> workerIds;
  final List<String> taskIds;

  const Group({
    required this.id,
    required this.name,
    required this.ownerId,
    required this.workerIds,
    required this.taskIds,
  });
}
