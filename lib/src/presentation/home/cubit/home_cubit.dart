import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taskmanage/src/domain/model/group.dart';
import 'package:taskmanage/src/domain/model/user.dart';
import 'package:taskmanage/src/domain/repo/group_repo.dart';
import 'package:taskmanage/src/domain/repo/user_repo.dart';
import 'package:taskmanage/src/presentation/navigation/cubit/navigation_cubit.dart';

import 'home_state.dart';

class HomeCubit extends Cubit<HomeState> {
  final IGroupRepo _groupRepo;
  final IUserRepo _userRepo;
  final void Function() _navigateAuth;

  HomeCubit({
    required User user,
    required IGroupRepo groupRepo,
    required IUserRepo userRepo,
    required NavigationCubit navigationCubit,
  })   : _groupRepo = groupRepo,
        _userRepo = userRepo,
        _navigateAuth = navigationCubit.navigatieAuth,
        super(HomeState(user: user)) {
    _initialEvent();
  }

  void _initialEvent() async {
    final groups = await _groupRepo.getGroups();
    emit(state.copyWith(groups: groups));
  }

  void onCreateGroupDialogTap() {
    emit(state.copyWith(createdGroupName: ''));
  }

  void onCreatedGroupNameChanged(String groupName) {
    emit(state.copyWith(createdGroupName: groupName));
  }

  void onCreateGroupTap() async {
    await _groupRepo.createGroup(groupName: state.createdGroupName);
    final groups = await _groupRepo.getGroups();
    emit(state.copyWith(groups: groups));
  }

  void onGroupTap({required Group group}) {}

  void onLogoutTap() async {
    await _userRepo.logout();
    _navigateAuth();
  }
}
