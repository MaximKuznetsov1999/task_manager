import 'package:taskmanage/src/domain/model/group.dart';
import 'package:taskmanage/src/domain/model/user.dart';

class HomeState {
  final User user;
  final List<Group> groups;
  final String createdGroupName;

  HomeState({
    required this.user,
    this.groups = const <Group>[],
    this.createdGroupName = '',
  });

  HomeState copyWith(
      {User? user, List<Group>? groups, String? createdGroupName}) {
    return HomeState(
      user: user ?? this.user,
      groups: groups ?? this.groups,
      createdGroupName: createdGroupName ?? this.createdGroupName,
    );
  }
}
