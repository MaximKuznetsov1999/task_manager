import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taskmanage/src/domain/model/group.dart';
import 'package:taskmanage/src/domain/model/user.dart';

import '../cubit/cubit.dart';

class HomeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeCubit, HomeState>(
      builder: (context, state) {
        return Scaffold(
          appBar: _createAppbar(
            context: context,
            title: state.user.name,
          ),
          body: SafeArea(
            child: Padding(
              padding: EdgeInsets.all(20),
              child: _createBody(user: state.user, groups: state.groups),
            ),
          ),
          floatingActionButton: _floatingButtons(),
        );
      },
    );
  }

  PreferredSizeWidget _createAppbar({
    required BuildContext context,
    required String title,
  }) {
    return AppBar(
      title: Text(title),
      actions: [
        IconButton(
          onPressed: BlocProvider.of<HomeCubit>(context).onLogoutTap,
          icon: Icon(Icons.logout),
        ),
      ],
    );
  }

  Widget _createBody({required User user, required List<Group> groups}) {
    final myGroups = groups.where((e) => e.ownerId == user.id).toList();
    final invitedToGroups = groups.where((e) => e.ownerId != user.id).toList();
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (myGroups.length != 0) ...[
            Text(
              'Мои группы:',
              style: TextStyle(fontSize: 20),
            ),
            SizedBox(height: 10),
            ...myGroups.map((e) => _createGroupButton(group: e)).toList(),
          ],
          if (invitedToGroups.length != 0) ...[
            Text(
              'Группы, в которые меня пригласили:',
              style: TextStyle(fontSize: 5),
            ),
            SizedBox(height: 10),
            ...invitedToGroups
                .map((e) => _createGroupButton(group: e))
                .toList(),
          ],
        ],
      ),
    );
  }

  Widget _createGroupButton({required Group group}) {
    return Builder(
      builder: (context) => ElevatedButton(
        onPressed: () =>
            BlocProvider.of<HomeCubit>(context).onGroupTap(group: group),
        child: Container(
          width: double.infinity,
          child: Text(group.name),
        ),
      ),
    );
  }

  Widget _floatingButtons() {
    return Builder(
      builder: (context) => Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          ElevatedButton(
            onPressed: () {
              BlocProvider.of<HomeCubit>(context).onCreateGroupDialogTap();
              showDialog(
                context: context,
                useRootNavigator: false,
                builder: (_) {
                  return BlocProvider.value(
                    value: BlocProvider.of<HomeCubit>(context),
                    child: Dialog(
                      child: Container(
                        height: 300,
                        width: 300,
                        padding: EdgeInsets.all(20),
                        color: Colors.white,
                        child: Column(
                          children: [
                            Text('Создать новую группу'),
                            Spacer(),
                            BlocBuilder<HomeCubit, HomeState>(
                              builder: (context, state) {
                                return TextField(
                                  controller: TextEditingController.fromValue(
                                    TextEditingValue(
                                      text: state.createdGroupName,
                                      selection: TextSelection.collapsed(
                                        offset: state.createdGroupName.length,
                                      ),
                                    ),
                                  ),
                                  onChanged: (value) {
                                    BlocProvider.of<HomeCubit>(context)
                                        .onCreatedGroupNameChanged(value);
                                  },
                                  decoration: InputDecoration(
                                    hintText: 'Наименование группы',
                                  ),
                                );
                              },
                            ),
                            Spacer(),
                            ElevatedButton(
                              onPressed: () {
                                BlocProvider.of<HomeCubit>(context)
                                    .onCreateGroupTap();
                                Navigator.of(context).pop();
                              },
                              child: Text('Создать'),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              );
            },
            child: Text('Создать группу'),
          ),
        ],
      ),
    );
  }
}
