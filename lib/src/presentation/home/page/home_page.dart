import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taskmanage/src/domain/model/user.dart';
import 'package:taskmanage/src/domain/repo/group_repo.dart';
import 'package:taskmanage/src/domain/repo/user_repo.dart';
import 'package:taskmanage/src/presentation/navigation/cubit/navigation_cubit.dart';

import 'home_widget.dart';
import '../cubit/home_cubit.dart';

const homePageKey = ValueKey('home_page');

class HomePage extends MaterialPage {
  HomePage({required User user})
      : super(
          key: homePageKey,
          child: BlocProvider(
            create: (context) => HomeCubit(
              user: user,
              groupRepo: RepositoryProvider.of<IGroupRepo>(context),
              userRepo: RepositoryProvider.of<IUserRepo>(context),
              navigationCubit: BlocProvider.of<NavigationCubit>(context),
            ),
            child: HomeWidget(),
          ),
        );
}
