class AuthState {
  final bool isAuthorization;
  final String name;
  final String email;
  final String password;
  final bool isLoading;
  final String? nameError;
  final String? emailError;
  final String? passwordError;

  AuthState({
    required this.isAuthorization,
    required this.name,
    required this.email,
    required this.password,
    required this.isLoading,
    this.nameError,
    this.emailError,
    this.passwordError,
  });

  AuthState copyWith({
    bool? isAuthorization,
    String? name,
    String? email,
    String? password,
    bool? isLoading,
  }) {
    return AuthState(
      isAuthorization: isAuthorization ?? this.isAuthorization,
      name: name ?? this.name,
      email: email ?? this.email,
      password: password ?? this.password,
      isLoading: isLoading ?? this.isLoading,
      nameError: this.nameError,
      emailError: this.emailError,
      passwordError: this.passwordError,
    );
  }

  AuthState copyWithNameError(String? nameError) {
    return AuthState(
      isAuthorization: this.isAuthorization,
      name: this.name,
      email: this.email,
      password: this.password,
      isLoading: this.isLoading,
      nameError: nameError,
      emailError: this.emailError,
      passwordError: this.passwordError,
    );
  }

  AuthState copyWithEmailError(String? emailError) {
    return AuthState(
      isAuthorization: this.isAuthorization,
      name: this.name,
      email: this.email,
      password: this.password,
      isLoading: this.isLoading,
      nameError: this.nameError,
      emailError: emailError,
      passwordError: this.passwordError,
    );
  }

  AuthState copyWithPasswordError(String? passwordError) {
    return AuthState(
      isAuthorization: this.isAuthorization,
      name: this.name,
      email: this.email,
      password: this.password,
      isLoading: this.isLoading,
      nameError: this.nameError,
      emailError: this.emailError,
      passwordError: passwordError,
    );
  }
}
