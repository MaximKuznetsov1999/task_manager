import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taskmanage/src/domain/exception/auth_exception.dart';
import 'package:taskmanage/src/domain/model/user.dart';
import 'package:taskmanage/src/domain/repo/user_repo.dart';
import 'package:taskmanage/src/presentation/navigation/cubit/navigation_cubit.dart';

import 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  final IUserRepo _userRepo;
  final void Function() _navigateVerification;
  final void Function({required User user}) _navigateHome;

  AuthCubit({
    required IUserRepo userRepo,
    required NavigationCubit navigationCubit,
  })   : _userRepo = userRepo,
        _navigateVerification = navigationCubit.navigateVerification,
        _navigateHome = navigationCubit.navigateHome,
        super(
          AuthState(
            isAuthorization: true,
            name: '',
            email: '',
            password: '',
            isLoading: false,
          ),
        );

  void onChangeName(String name) {
    emit(state.copyWith(name: name));
    emit(state.copyWithNameError(null));
  }

  void onChangeEmail(String email) {
    emit(state.copyWith(email: email));
    emit(state.copyWithEmailError(null));
  }

  void onChangePassword(String password) {
    emit(state.copyWith(password: password));
    emit(state.copyWithPasswordError(null));
  }

  void onEnterButtonTap() async {
    emit(state.copyWith(isLoading: true));
    emit(state.copyWithNameError(null));
    emit(state.copyWithEmailError(null));
    emit(state.copyWithPasswordError(null));
    if (!state.isAuthorization && state.name.trim().length == 0) {
      emit(state.copyWithNameError('Введите имя'));
      emit(state.copyWith(isLoading: false));
      return;
    }
    try {
      if (state.isAuthorization) {
        await _userRepo.login(
          email: state.email,
          password: state.password,
        );
      } else {
        await _userRepo.createAccount(
          email: state.email,
          name: state.name,
          password: state.password,
        );
      }
    } catch (e) {
      if (e.toString().toLowerCase().contains('email')) {
        emit(state.copyWithEmailError('Неверный email'));
      } else if (e.toString().toLowerCase().contains('password')) {
        emit(state.copyWithPasswordError('Неверный пароль'));
      } else {
        emit(state.copyWithEmailError('Пользователь с таким email не найден'));
      }
    } finally {
      emit(state.copyWith(isLoading: false));
    }

    if (state.isAuthorization &&
        state.emailError == null &&
        state.passwordError == null) {
      User? user;
      try {
        user = await _userRepo.getCurrentUser();
        if (user == null) {
          emit(
              state.copyWithEmailError('Пользователь с таким email не найден'));
        }
      } catch (e) {
        if (e is EmailIsNotVerifiedException) {
          emit(state.copyWithEmailError('Email не подтвержден'));
        }
      }
      if (user != null) {
        _navigateHome(user: user);
      }
    } else if (state.nameError == null &&
        state.emailError == null &&
        state.passwordError == null) {
      _navigateVerification();
    }
  }

  void onChangeAuthTypeButtonTap() {
    emit(state.copyWith(isAuthorization: !state.isAuthorization));
    emit(state.copyWithEmailError(null));
    emit(state.copyWithPasswordError(null));
  }
}
