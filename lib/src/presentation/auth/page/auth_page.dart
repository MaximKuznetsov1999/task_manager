import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taskmanage/src/domain/repo/user_repo.dart';
import 'package:taskmanage/src/presentation/navigation/cubit/navigation_cubit.dart';

import '../cubit/auth_cubit.dart';
import 'auth_widget.dart';

const authPageKey = ValueKey('auth_page');

class AuthPage extends MaterialPage {
  AuthPage()
      : super(
          key: authPageKey,
          child: BlocProvider(
            create: (context) => AuthCubit(
              userRepo: RepositoryProvider.of<IUserRepo>(context),
              navigationCubit: BlocProvider.of<NavigationCubit>(context),
            ),
            child: AuthWidget(),
          ),
        );
}
