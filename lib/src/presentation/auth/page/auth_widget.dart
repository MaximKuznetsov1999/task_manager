import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taskmanage/src/presentation/auth/auth.dart';

class AuthWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _createAppBar(),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.center,
                child: _createFormWidget(),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: _createChangeTypeAuthButton(),
              )
            ],
          ),
        ),
      ),
    );
  }

  PreferredSizeWidget _createAppBar() {
    return AppBar(
      title: BlocBuilder<AuthCubit, AuthState>(
        buildWhen: (previous, current) {
          return previous.isAuthorization != current.isAuthorization;
        },
        builder: (context, state) {
          return Text(
            state.isAuthorization ? 'Авторизация' : 'Регистрация',
          );
        },
      ),
    );
  }

  Widget _createFormWidget() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _createNameWidget(),
        _createEmailWidget(),
        _createPasswordWidget(),
        _createEnterButton(),
      ],
    );
  }

  Widget _createNameWidget() {
    return BlocBuilder<AuthCubit, AuthState>(
      buildWhen: (previous, current) {
        return previous.isAuthorization != current.isAuthorization ||
            previous.name != current.name ||
            previous.nameError != current.nameError;
      },
      builder: (context, state) {
        if (state.isAuthorization) return Container();
        return TextField(
          controller: TextEditingController.fromValue(
            TextEditingValue(
              text: state.name,
              selection: TextSelection.fromPosition(
                TextPosition(
                  offset: state.name.length,
                ),
              ),
            ),
          ),
          onChanged: (value) {
            BlocProvider.of<AuthCubit>(context).onChangeName(value);
          },
          decoration: InputDecoration(
            hintText: 'Имя',
            errorText: state.nameError,
          ),
        );
      },
    );
  }

  Widget _createEmailWidget() {
    return BlocBuilder<AuthCubit, AuthState>(
      buildWhen: (previous, current) {
        return previous.email != current.email ||
            previous.emailError != current.emailError;
      },
      builder: (context, state) {
        return TextField(
          controller: TextEditingController.fromValue(
            TextEditingValue(
              text: state.email,
              selection: TextSelection.fromPosition(
                TextPosition(
                  offset: state.email.length,
                ),
              ),
            ),
          ),
          onChanged: (value) {
            BlocProvider.of<AuthCubit>(context).onChangeEmail(value);
          },
          decoration: InputDecoration(
            hintText: 'Email',
            errorText: state.emailError,
          ),
        );
      },
    );
  }

  Widget _createPasswordWidget() {
    return BlocBuilder<AuthCubit, AuthState>(
      buildWhen: (previous, current) {
        return previous.password != current.password ||
            previous.passwordError != current.passwordError;
      },
      builder: (context, state) {
        return TextField(
            controller: TextEditingController.fromValue(
              TextEditingValue(
                text: state.password,
                selection: TextSelection.fromPosition(
                  TextPosition(
                    offset: state.password.length,
                  ),
                ),
              ),
            ),
            obscureText: true,
            onChanged: (value) {
              BlocProvider.of<AuthCubit>(context).onChangePassword(value);
            },
            decoration: InputDecoration(
              hintText: 'Пароль',
              errorText: state.passwordError,
            ));
      },
    );
  }

  Widget _createEnterButton() {
    return Container(
      width: double.infinity,
      child: BlocBuilder<AuthCubit, AuthState>(
        buildWhen: (previous, current) {
          return previous.isAuthorization != current.isAuthorization ||
              previous.isLoading != current.isLoading;
        },
        builder: (context, state) {
          return ElevatedButton(
            onPressed: BlocProvider.of<AuthCubit>(context).onEnterButtonTap,
            child: state.isLoading
                ? CircularProgressIndicator()
                : Text(state.isAuthorization ? 'Войти' : 'Создать аккаунт'),
          );
        },
      ),
    );
  }

  Widget _createChangeTypeAuthButton() {
    return BlocBuilder<AuthCubit, AuthState>(
      buildWhen: (previous, current) {
        return previous.isAuthorization != current.isAuthorization;
      },
      builder: (context, state) {
        return TextButton(
          onPressed:
              BlocProvider.of<AuthCubit>(context).onChangeAuthTypeButtonTap,
          child: Text(
            state.isAuthorization ? 'Создать аккаунт' : 'Уже есть аккаунт',
          ),
        );
      },
    );
  }
}
