import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taskmanage/src/domain/model/task.dart';

import 'task_widget.dart';
import '../cubit/task_cubit.dart';

const taskPageKey = ValueKey('task_page');

class TaskPage extends MaterialPage {
  TaskPage({required Task task})
      : super(
          key: taskPageKey,
          child: BlocProvider(
            create: (_) => TaskCubit(task: task),
            child: TaskWidget(),
          ),
        );
}
