import 'package:taskmanage/src/domain/model/task.dart';

abstract class TaskState {}

class InitialState extends TaskState {
  final Task task;

  InitialState({required this.task});
}
