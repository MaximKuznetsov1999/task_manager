import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taskmanage/src/domain/model/task.dart';

import 'task_state.dart';

class TaskCubit extends Cubit<TaskState> {
  TaskCubit({required Task task}) : super(InitialState(task: task));
}
