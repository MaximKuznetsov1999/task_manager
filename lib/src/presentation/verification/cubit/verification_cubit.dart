import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taskmanage/src/domain/repo/user_repo.dart';
import 'package:taskmanage/src/presentation/navigation/cubit/navigation_cubit.dart';

import 'verification_state.dart';

class VerificationCubit extends Cubit<VerificationState> {
  final IUserRepo _userRepo;
  final void Function() _navigateAuth;

  VerificationCubit({
    required IUserRepo userRepo,
    required NavigationCubit navigationCubit,
  })   : _userRepo = userRepo,
        _navigateAuth = navigationCubit.navigatieAuth,
        super(VerificationState());

  void onLoginTap() async {
    await _userRepo.logout();
    _navigateAuth();
  }
}
