import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit/cubit.dart';

class VerificationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _createAppbar(),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: _createBody(),
        ),
      ),
    );
  }

  PreferredSizeWidget _createAppbar() {
    return AppBar(
      title: Text('Верификация'),
    );
  }

  Widget _createBody() {
    return BlocBuilder<VerificationCubit, VerificationState>(
      builder: (context, state) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'На вашу почту отправлено письмо с подтверждением email адреса\nПожалуйста, перейдите по ссылке в письме для подтверждения, затем войдите в аккаунт',
              textAlign: TextAlign.center,
            ),
            ElevatedButton(
              onPressed: BlocProvider.of<VerificationCubit>(context).onLoginTap,
              child: Text('Войти в аккаунт'),
            ),
          ],
        );
      },
    );
  }
}
