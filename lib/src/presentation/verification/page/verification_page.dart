import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taskmanage/src/domain/repo/user_repo.dart';

import 'package:taskmanage/src/presentation/navigation/cubit/navigation_cubit.dart';
import 'verification_widget.dart';
import '../cubit/verification_cubit.dart';

const verificationPageKey = ValueKey('verification_page');

class VerificationPage extends MaterialPage {
  VerificationPage()
      : super(
          key: verificationPageKey,
          child: BlocProvider(
            create: (context) => VerificationCubit(
              userRepo: RepositoryProvider.of<IUserRepo>(context),
              navigationCubit: BlocProvider.of<NavigationCubit>(context),
            ),
            child: VerificationWidget(),
          ),
        );
}
