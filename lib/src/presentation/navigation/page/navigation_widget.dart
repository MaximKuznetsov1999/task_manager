import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubit/cubit.dart';
import 'package:taskmanage/src/presentation/auth/auth.dart' as page;
import 'package:taskmanage/src/presentation/verification/verification.dart'
    as page;
import 'package:taskmanage/src/presentation/home/home.dart' as page;
import 'package:taskmanage/src/presentation/group/group.dart' as page;
import 'package:taskmanage/src/presentation/task/task.dart' as page;

class NavigationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NavigationCubit, NavigationState>(
      builder: (context, state) {
        return Navigator(
          pages: _createPagesFromState(state: state),
          onPopPage: (route, result) => true,
        );
      },
    );
  }

  List<Page> _createPagesFromState({required NavigationState state}) {
    switch (state.runtimeType) {
      case AuthState:
        return _createPagesFromAuthState(state: state as AuthState);
      case VerificationState:
        return _createPagesFromVerificationState(
          state: state as VerificationState,
        );
      case HomeState:
        return _createPagesFromHomeState(state: state as HomeState);
      case GroupState:
        return _createPagesFromGroupState(state: state as GroupState);
      case TaskState:
        return _createPagesFromTaskState(state: state as TaskState);
    }
    return [];
  }

  List<Page> _createPagesFromAuthState({required AuthState state}) {
    return [page.AuthPage()];
  }

  List<Page> _createPagesFromVerificationState(
      {required VerificationState state}) {
    return [page.VerificationPage()];
  }

  List<Page> _createPagesFromHomeState({required HomeState state}) {
    return [page.HomePage(user: state.user)];
  }

  List<Page> _createPagesFromGroupState({required GroupState state}) {
    return [
      page.HomePage(user: state.user),
      page.GroupPage(group: state.group)
    ];
  }

  List<Page> _createPagesFromTaskState({required TaskState state}) {
    return [
      page.HomePage(user: state.user),
      page.GroupPage(group: state.group),
      page.TaskPage(task: state.task),
    ];
  }
}
