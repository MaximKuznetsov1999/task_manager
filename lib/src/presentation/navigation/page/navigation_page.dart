import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taskmanage/src/domain/model/user.dart';

import 'navigation_widget.dart';
import '../cubit/navigation_cubit.dart';

class NavigationPage extends StatelessWidget {
  final User? _user;

  NavigationPage({required User? user}) : _user = user;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => NavigationCubit(user: _user),
      child: NavigationWidget(),
    );
  }
}
