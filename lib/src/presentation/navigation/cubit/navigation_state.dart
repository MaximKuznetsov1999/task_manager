import 'package:taskmanage/src/domain/model/group.dart';
import 'package:taskmanage/src/domain/model/task.dart';
import 'package:taskmanage/src/domain/model/user.dart';

abstract class NavigationState {}

class AuthState extends NavigationState {}

class VerificationState extends NavigationState {}

class HomeState extends NavigationState {
  final User user;

  HomeState({required this.user});
}

class GroupState extends NavigationState {
  final User user;
  final Group group;

  GroupState({required this.user, required this.group});
}

class TaskState extends NavigationState {
  final User user;
  final Group group;
  final Task task;

  TaskState({required this.user, required this.group, required this.task});
}
