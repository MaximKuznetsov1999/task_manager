import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taskmanage/src/domain/model/group.dart';
import 'package:taskmanage/src/domain/model/task.dart';
import 'package:taskmanage/src/domain/model/user.dart';

import 'navigation_state.dart';

class NavigationCubit extends Cubit<NavigationState> {
  User? _user;
  Group? _group;

  NavigationCubit({required User? user})
      : super(
          _createInitialState(user),
        );

  static NavigationState _createInitialState(User? user) {
    if (user != null) return HomeState(user: user);
    return AuthState();
  }

  void navigatieAuth() {
    _user = null;
    _group = null;
    emit(AuthState());
  }

  void navigateVerification() {
    emit(VerificationState());
  }

  void navigateHome({required User user}) {
    _user = user;
    _group = null;
    emit(HomeState(user: user));
  }

  void navigateGroup({required Group group}) {
    _group = group;
    emit(GroupState(user: _user!, group: group));
  }

  void navigateTask(Task task) {
    emit(TaskState(user: _user!, group: _group!, task: task));
  }
}
