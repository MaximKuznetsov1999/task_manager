import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:taskmanage/src/domain/model/group.dart';

import 'group_widget.dart';
import '../cubit/group_cubit.dart';

const groupPageKey = ValueKey('group_page');

class GroupPage extends MaterialPage {
  GroupPage({required Group group})
      : super(
          key: groupPageKey,
          child: BlocProvider(
            create: (_) => GroupCubit(group: group),
            child: GroupWidget(),
          ),
        );
}
