import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:taskmanage/src/domain/model/group.dart';
import 'group_state.dart';

class GroupCubit extends Cubit<GroupState> {
  GroupCubit({required Group group}) : super(InitialState(group: group));
}
