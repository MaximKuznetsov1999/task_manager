import 'package:taskmanage/src/domain/model/group.dart';

abstract class GroupState {}

class InitialState extends GroupState {
  final Group group;

  InitialState({required this.group});
}
