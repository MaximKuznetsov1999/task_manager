import 'package:cloud_firestore/cloud_firestore.dart' as fb;
import 'package:firebase_auth/firebase_auth.dart' as fb;
import 'package:taskmanage/src/domain/exception/auth_exception.dart';
import 'package:taskmanage/src/domain/model/user.dart';
import 'package:taskmanage/src/firebase_store/src/mapper/user_mapper.dart';

class UserFirestore {
  final fb.FirebaseAuth _firebaseAuth;
  final fb.FirebaseFirestore _firestore;

  const UserFirestore({
    required fb.FirebaseAuth firebaseAuth,
    required fb.FirebaseFirestore firestore,
  })   : _firebaseAuth = firebaseAuth,
        _firestore = firestore;

  Future<User?> getCurrentUser() async {
    if (_firebaseAuth.currentUser == null) return null;
    if (!(_firebaseAuth.currentUser?.emailVerified ?? false))
      throw EmailIsNotVerifiedException();
    final id = _firebaseAuth.currentUser?.uid;
    final query =
        await _firestore.collection('user').where('id', isEqualTo: id).get();
    if (query.docs.isEmpty) return null;
    return UserMapper.fromMap(query.docs.first.data());
  }

  Future<void> createAccount({
    required String email,
    required String name,
    required String password,
  }) async {
    final userCredential = await _firebaseAuth.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );
    await _firebaseAuth.currentUser?.sendEmailVerification();
    final id = userCredential.user?.uid;
    if (id == null) return;
    await _firestore.collection('user').add({
      'id': id,
      'name': name,
      'email': email,
    });
  }

  bool isVerified() {
    return _firebaseAuth.currentUser?.emailVerified ?? false;
  }

  Future<void> login({required String email, required String password}) async {
    await _firebaseAuth.signInWithEmailAndPassword(
      email: email,
      password: password,
    );
  }

  Future<void> logout() async {
    await _firebaseAuth.signOut();
  }

  Future<User?> getUserById(String userId) async {
    final snapshot = await _firestore
        .collection('user')
        .where('id', isEqualTo: userId)
        .get();
    if (snapshot.docs.isEmpty) return null;
    return UserMapper.fromMap(snapshot.docs.first.data());
  }

  Future<User?> getUserByEmail(String email) async {
    final snapshot = await _firestore
        .collection('user')
        .where('email', isEqualTo: email)
        .get();
    if (snapshot.docs.isEmpty) return null;
    return UserMapper.fromMap(snapshot.docs.first.data());
  }
}
