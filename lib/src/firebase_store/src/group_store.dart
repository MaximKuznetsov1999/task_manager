import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:taskmanage/src/domain/model/group.dart';
import 'package:taskmanage/src/firebase_store/src/mapper/group_mapper.dart';

class GroupFirestore {
  final FirebaseAuth _firebaseAuth;
  final FirebaseFirestore _firestore;

  const GroupFirestore({
    required FirebaseAuth firebaseAuth,
    required FirebaseFirestore firestore,
  })   : _firebaseAuth = firebaseAuth,
        _firestore = firestore;

  Future<void> createGroup({required String groupName}) async {
    final userId = _firebaseAuth.currentUser!.uid;
    final docRef = await _firestore.collection('group').add({
      'name': groupName,
      'ownerId': userId,
      'workerIds': <String>[],
      'taskIds': <String>[],
    });
    await docRef.update({'id': docRef.id});
  }

  Future<List<Group>> getGroups() async {
    final userId = _firebaseAuth.currentUser?.uid;
    final groups = <Group>[];
    final snapshotMyGroups = await _firestore
        .collection('group')
        .where('ownerId', isEqualTo: userId)
        .get();
    final snapshotOtherGroups = await _firestore
        .collection('group')
        .where('workerIds', arrayContains: userId)
        .get();
    snapshotMyGroups.docs.forEach((e) {
      groups.add(GroupMapper.fromMap(e.data()));
    });
    snapshotOtherGroups.docs.forEach((e) {
      groups.add(GroupMapper.fromMap(e.data()));
    });
    return groups;
  }

  Future<void> updateGroupName(
      String groupId, String userId, String name) async {
    final snapshots = await _firestore
        .collection('group')
        .where('id', isEqualTo: groupId)
        .where('ownerId', isEqualTo: userId)
        .get();
    if (snapshots.docs.isEmpty) return;
    await snapshots.docs.first.reference.update({'name': name});
  }

  Future<void> removeWorker(String groupId, String workerId) async {
    final snapshot = await _firestore
        .collection('group')
        .where('id', isEqualTo: groupId)
        .get();
    if (snapshot.docs.isEmpty) return;
    final doc = snapshot.docs.first;
    final workerIds = doc.data()['workerIds'] as List<String>;
    workerIds.remove(workerId);
    await doc.reference.update({'workerIds': workerIds});
  }

  Future<void> removeGroup(String groupId) async {
    final snapshot = await _firestore
        .collection('group')
        .where('id', isEqualTo: groupId)
        .get();
    if (snapshot.docs.isEmpty) return;
    final doc = snapshot.docs.first;
    await doc.reference.delete();
  }
}
