import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:taskmanage/src/domain/model/invite.dart';
import 'package:taskmanage/src/firebase_store/src/mapper/invite_mapper.dart';

class InviteFirestore {
  final FirebaseFirestore _firestore;

  const InviteFirestore({required FirebaseFirestore firestore})
      : _firestore = firestore;

  Future<List<Invite>> getInvites(String userId) async {
    final snapshot = await _firestore
        .collection('invite')
        .where('ownerId', isEqualTo: userId)
        .get();
    return snapshot.docs
        .map<Invite>((e) => InviteMapper.fromMap(e.data()))
        .toList();
  }

  Future<void> createInvite(
    String ownerId,
    String workerId,
    String groupId,
    String groupName,
  ) async {
    final inviteReference = await _firestore.collection('invite').add(
      {
        'ownerId': ownerId,
        'workerId': workerId,
        'groupId': groupId,
        'groupName': groupName,
      },
    );
    await inviteReference.update({'id': inviteReference.id});
  }
}
