import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:taskmanage/src/domain/model/task.dart';
import 'package:taskmanage/src/firebase_store/src/mapper/task_mapper.dart';

class TaskFirestore {
  final FirebaseFirestore _firestore;

  const TaskFirestore({required FirebaseFirestore firestore})
      : _firestore = firestore;

  Future<List<Task>> getTasks(String groupId, String userId) async {
    final snapshot = await _firestore
        .collection('task')
        .where('workerId', isEqualTo: userId)
        .get();
    return snapshot.docs
        .map<Task>((e) => TaskMapper.fromMap(e.data()))
        .toList();
  }

  Future<void> createTask(
    String name,
    String workerId,
    String description,
  ) async {
    final taskReference = await _firestore.collection('task').add({
      'name': name,
      'workerId': workerId,
      'description': description,
      'status': false,
      'dateTime': DateTime.now(),
    });
    await taskReference.update({'id': taskReference.id});
  }

  Future<void> updateTask({
    required String id,
    String? name,
    bool? status,
    String? description,
    String? executorId,
  }) async {
    final snapshot =
        await _firestore.collection('task').where('id', isEqualTo: id).get();
    if (snapshot.docs.isEmpty) return;
    final doc = snapshot.docs.first;
    await doc.reference.update({
      if (name != null) 'name': name,
      if (status != null) 'status': status,
      if (description != null) 'description': description,
      if (executorId != null) 'executorId': executorId,
    });
  }

  Future<void> removeTask(String taskId) async {
    final snapshot = await _firestore
        .collection('task')
        .where('id', isEqualTo: taskId)
        .get();
    if (snapshot.docs.isEmpty) return;
    await snapshot.docs.first.reference.delete();
  }
}
