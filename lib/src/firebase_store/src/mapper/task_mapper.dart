import 'package:taskmanage/src/domain/model/task.dart';

extension TaskMapper on Task {
  static Task fromMap(Map map) {
    return Task(
      id: map['id'] as String,
      name: map['name'] as String,
      workerId: map['workerId'] as String,
      description: map['description'] as String,
      status: map['status'] as bool,
      dateTime: map['dateTime'] as DateTime,
    );
  }
}
