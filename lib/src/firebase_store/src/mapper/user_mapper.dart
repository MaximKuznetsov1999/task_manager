import 'package:taskmanage/src/domain/model/user.dart';

extension UserMapper on User {
  static User fromMap(Map map) {
    return User(
      id: map['id'] as String,
      name: map['name'] as String,
      email: map['email'] as String,
    );
  }
}
