import 'package:taskmanage/src/domain/model/invite.dart';

extension InviteMapper on Invite {
  static Invite fromMap(Map map) {
    return Invite(
      id: map['id'] as String,
      ownerId: map['ownerId'] as String,
      workerId: map['workerId'] as String,
      groupId: map['groupId'] as String,
      groupName: map['groupName'] as String,
    );
  }
}
