import 'package:taskmanage/src/domain/model/group.dart';

extension GroupMapper on Group {
  static Group fromMap(Map map) {
    return Group(
      id: map['id'] as String,
      name: map['name'] as String,
      ownerId: map['ownerId'] as String,
      workerIds: (map['workerIds'] as List).cast<String>(),
      taskIds: (map['taskIds'] as List).cast<String>(),
    );
  }
}
